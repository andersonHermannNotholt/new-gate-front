const path = require('path');

const cfg = {
  config: path.resolve(__dirname, 'config.js'),
};

if (process.env.CWD) {
  cfg['models-path'] = process.env.CWD;
  cfg['seeders-path'] = path.resolve(process.env.CWD, 'seeders');
  cfg['migrations-path'] = path.resolve(process.env.CWD, 'migrations');
}

module.exports = cfg;
