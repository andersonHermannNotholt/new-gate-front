/**
 * Imprime uma mensagem de boas vindas
 * @param {Express|Featherjs} app
 */
export default function setupHello(app) {
  app.use((req, res, next) => {
    if (req.path === '/') {
      res.send('Hello Node.js Server!');
    }
    next();
  });
}
