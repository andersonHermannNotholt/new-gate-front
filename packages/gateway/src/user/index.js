import UserService from './user.service';
import PasswordResetService from './passwordReset.service';
import PasswordChangeService from './passwordChange.service';
import EmailSubscriptionService from './emailSubscription.service';
import UserModel from './user.model';
import userHooks from './user.hooks';

/**
  * User domain
  *
  * @module User
  */
export default function setupUser(app) {
  const model = UserModel(app);

  // Initialize our services with any options it requires
  app.use('/users/passwordResets', PasswordResetService(app));
  app.use('/users/passwordChanges', PasswordChangeService(app));
  app.use('/users/emailSubscriptions', EmailSubscriptionService(app));

  // Initialize our service with any options it requires
  app.use('/users', UserService({ Model: model }));

  // Register hooks
  app.service('users').hooks(userHooks);
}
