/* eslint-disable no-unused-vars */


module.exports = {
  up: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Users DROP FOREIGN KEY Users_managerId_foreign_idx, MODIFY managerId BIGINT UNSIGNED NULL;',
  ),
  down: (QI, Sequelize) => QI.sequelize.query(
    'ALTER TABLE Users ADD CONSTRAINT Users_managerId_foreign_idx FOREIGN KEY(managerId) REFERENCES Users(id), MODIFY managerId INT(11) NULL;',
  ),
};
