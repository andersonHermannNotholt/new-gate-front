/* istanbul ignore file */
import bcrypt from 'bcrypt';
import { logger } from '@/logger';

export default async function passwordHash(context) {
  const { data } = context;
  if (data.password) {
    const saltRounds = 10;
    context.data.password = await bcrypt.hash(data.password, saltRounds);
    logger.debug('password hashed %s for user %s', context.data.password, context.data.email);
  }
  return context;
}
