/* eslint-disable camelcase */
import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';
import axios from 'axios';
import PasswordResetService from './passwordReset.service';

jest.mock('axios');

describe('passwordReset.service create', () => {
  let app;

  beforeEach(() => {
    app = feathers();
    app.configure(configuration());
    app.serverToken = {
      access_token: 'foo',
    };
  });

  it('send orsegups resetpassword request and return result', async () => {
    const email = 'foo@bar';
    const message = 'Senha resetada com sucesso';
    const orsegupsId = app.get('orsegupsId');
    const { access_token } = app.serverToken;
    const endpoint = `${orsegupsId.uri}/user/resetpassword?access_token=${access_token}`;
    const status = 200;
    axios.put = jest.fn(() => Promise.resolve({
      data: { message, data: null },
      status,
    }));
    const service = PasswordResetService(app);
    const data = { email, status };
    const result = await service.create(data);
    expect(axios.put).toBeCalledWith(
      endpoint,
      { email },
      {
        withCredentials: true,
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );
    expect(result).toMatchObject({
      id: email,
      message,
      status,
    });
  });
});
