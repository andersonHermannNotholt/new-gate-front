/* istanbul ignore file */
/**
  * FileRequest domain
  *
  * @module FileRequest
  */
import uniqid from 'uniqid';
import { logger } from '@/logger';

function createFileRequestService(app) {
  const { httpRequest } = app;
  const create = async (data) => {
    const { url } = data;
    const settings = {
      method: 'get',
      responseType: 'arraybuffer',
      url,
    };
    logger.debug('sending %s to %s', settings.method, url);
    // FIXME: remove this please
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
    try {
      const axRes = await httpRequest(settings);
      // FIXME: remove this please
      delete process.env.NODE_TLS_REJECT_UNAUTHORIZED;
      return {
        id: uniqid(),
        base64: Buffer.from(axRes.data, 'binary').toString('base64'),
        headers: axRes.headers,
      };
    } catch (err) {
      logger.error(
        'error sending %s to %s, details: %s',
        settings.method,
        url,
        err.response.data,
      );
      throw err;
    }
  };

  return { create };
}

/**
 * Configure fileRequest domain with services and hooks
 *
 * @name FileRequestSetup
 * @memberof module:FileRequest
 * @param {Feathers} app
 */
export default function FileRequestSetup(app) {
  // Initialize our service with any options it requires
  const fileRequestService = createFileRequestService(app);

  // Register service and hook
  app.use('/fileRequests', fileRequestService);
}
