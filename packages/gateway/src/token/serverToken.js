import Axios from 'axios';
import { logger } from '@/logger';

export function isValid(credential) {
  if (typeof credential === 'object' && credential.createdAt && credential.expires_in) {
    const expireDate = new Date(credential.createdAt.valueOf());
    expireDate.setMilliseconds(
      expireDate.getMilliseconds() + credential.expires_in * 1000,
    );
    return expireDate > new Date();
  }
  return false;
}

export default function setupServerToken(app) {
  async function updateToken() {
    const orsegupsId = app.get('orsegupsId');
    const postOptions = {
      auth: {
        username: orsegupsId.username,
        password: orsegupsId.password,
      },
    };
    const res = await Axios.post(
      `${orsegupsId.uri}/oauth/token?grant_type=client_credentials`,
      null,
      postOptions,
    );
    app.serverToken = {
      ...res.data,
      createdAt: new Date(),
    };
    logger.debug('server token updated, expires in %s', app.serverToken.expires_in);
  }

  app.jobs.register('refreshToken', updateToken);

  if (!isValid(app.serverToken)) {
    logger.debug('server token not found or invalid, updating');
    return updateToken();
  }

  return Promise.resolve();
}
