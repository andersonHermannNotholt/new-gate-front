/* istanbul ignore file */
import { logger } from '@/logger';

/**
 * A hook to update a record instead of creating a new one, based on an array of keys
 *
 * @export updateIfExists
 * @param {Array} updateByKeys attribute keys to look for duplicates
 * @param {Boolean} patch patch instead of update model
 * @returns {Function} hook function
 */
export default function updateIfExists(updateByKeys, patch = false) {
  return (context) => {
    const { service, data } = context;
    if (updateByKeys && updateByKeys.length > 0) {
      const primaryKeyField = service.id;

      const query = {};
      updateByKeys.forEach((updateByKey) => {
        if (data[updateByKey]) {
          query[updateByKey] = data[updateByKey];
        }
      });

      return service.find({ query }).then((result) => {
        if (result.length === 0) {
          logger.debug(`No existing record found in service with key(s): '${updateByKeys.join(', ')}'`);
        } else {
          logger.debug(`Existing record found in service with keys(s): '${updateByKeys.join(', ')}' updating...`);
          if (patch) {
            service.patch(result[0][primaryKeyField], data);
          } else {
            service.update(result[0][primaryKeyField], data);
          }
          [context.result] = result;
        }
        return Promise.resolve(context);
      });
    }
    return Promise.resolve(context);
  };
}
