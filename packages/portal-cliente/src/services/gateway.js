import axios from 'axios';

export default {
  get(endpoint = '') {
    const url = process.env.VUE_APP_GATEWAY_URI + endpoint;
    return axios.get(url);
  },
};
