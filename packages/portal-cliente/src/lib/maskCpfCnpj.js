export default function maskCpfCnpj(input, options = {}) {
  const { withLabel = false } = options;
  let type;
  let value;
  if (input.length > 11) {
    type = 'CNPJ';
    value = input.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, '$1.$2.$3/$4-$5');
  } else {
    type = 'CPF';
    value = input.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');
  }

  if (withLabel) {
    value = `${type} ${value}`;
  }
  return value;
}
