/* istanbul ignore file */
import interleave from '@/lib/interleave';

const state = {
  contact: null,
  contacts: null,
  searchResult: null,
};
const mutations = {
  setContact(st, contact) {
    st.contact = contact;
  },
  setContacts(st, contacts) {
    st.contacts = contacts;
  },
  setSearchResult(st, result) {
    st.searchResult = result;
  },
};
const getters = {
  contactTypes() {
    return [
      {
        label: 'Sugestão',
        value: 11715124,
      }, {
        label: 'Orçamento',
        value: 11715150,
      }, {
        label: 'Reclamação',
        value: 11715151,
      }, {
        label: 'Dúvida',
        value: 12429911,
      }, {
        label: 'Atualização cadastral',
        value: 12430375,
      }, {
        label: 'Solicitação',
        value: 127514862,
      }, {
        label: 'Agrupamento de CNPJ',
        value: 773898952,
      }, {
        label: 'Elogio',
        value: 1028885044,
      }, {
        label: 'Outros',
        value: 29568153,
      },
    ];
  },
};
const actions = {
  searchContact({ commit }, text = '') {
    const contactFilter = (contact) => {
      const target = text.toUpperCase();
      const props = [
        'categoria',
        'atendente',
        'codigoTarefa',
        'contato',
        'descricao',
        'link',
        'neoid',
        'solicitante',
      ];
      const checkProps = p => contact[p]
        && typeof contact[p] === 'string'
        && contact[p].toUpperCase().match(target);
      return props.some(checkProps);
    };
    const result = state.contacts.filter(contactFilter);
    commit(
      'setSearchResult',
      result.length > 30 ? result.slice(0, 30) : result,
    );
  },
  filterContacts({ commit, state: st }, typeId) {
    const filterContactsByType = (contact) => {
      // bypass filter if typeId is undefined
      if (!typeId) return true;

      // Old regex description filter
      // const { descricao = '' } = contact;
      // const { label } = gt.contactTypes.find(
      //   ({ value }) => value === typeId,
      // );
      // const regex = new RegExp(`^Tipo\\sdo\\sContato:\\s${label}`, 'm');
      // return descricao.match(regex);
      return contact.type === typeId;
    };
    const filteredContacts = st.contacts.filter(filterContactsByType);
    commit('setSearchResult', filteredContacts.slice(0, 30));
  },
  loadContact({ commit, state: st }, id) {
    const result = st.contacts.filter(
      c => c.codigoTarefa === String(id),
    );
    if (result.length === 1) {
      commit('setContact', result[0]);
    } else {
      throw Error('contact not found');
    }
  },
  async loadContacts({ dispatch, rootState, commit }) {
    const urlSol = '/portalcliente/v1/listaSolicitacoes';
    const urlRec = '/portalcliente/v1/listaReclamacoes';
    const { currentAccount, currentUser } = rootState.session;
    const { cpfCnpjOwner, contractCode, userOwnerId } = currentAccount;
    const params = {
      cgcCpf: cpfCnpjOwner,
      contractCode,
      admin: userOwnerId === currentUser.id,
    };
    const method = 'get';
    const onError = () => {
      this.$notify('Erro ao carregar contatos.');
    };
    try {
      const [resSol, resRec] = await Promise.all([
        dispatch('mobileRequests/create', { url: urlSol, params, method }, { root: true }),
        dispatch('mobileRequests/create', { url: urlRec, params, method }, { root: true }),
      ]);
      if (resSol.data.status === 100 && resRec.data.status === 100) {
        const attachType = (arr, type) => arr.map(v => ({ ...v, type }));
        const mergedContacts = interleave(
          attachType(resSol.data.ret, 'sol'),
          attachType(resRec.data.ret, 'rec'),
        );
        commit('setContacts', mergedContacts);
        commit('setSearchResult', mergedContacts.slice(0, 30));
      } else {
        onError();
      }
    } catch (e) {
      onError(e);
    }
  },
  async onAddClick({ dispatch, getters: gt, rootState }, payload) {
    const {
      contact,
      onSuccess = () => { },
    } = payload;
    const { cpfCnpjOwner } = rootState.session.currentAccount;
    const data = {
      nome: contact.name,
      email: contact.email,
      mensagem: contact.message,
      telefone: contact.phone,
      tipoContato: contact.contactType,
      cpfCnpj: cpfCnpjOwner,
    };
    const contactTypeLabel = gt.contactTypes.find(
      cont => cont.value === contact.contactType,
    ).label;
    const url = '/portalcliente/v1/salvarcontato';
    const method = 'post';
    const onError = () => {
      this.$notify(`Erro ao enviar ${contactTypeLabel}.`);
    };
    try {
      const res = await dispatch('mobileRequests/create', { url, data, method }, { root: true });
      if (res.data.status === 100) {
        this.$notify(
          `${contactTypeLabel} enviado(a) para análise. Em breve enviaremos um email com a confirmação.`,
          'success',
        );
        onSuccess();
      } else {
        onError();
      }
    } catch (e) {
      onError(e);
    }
  },
};
export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
