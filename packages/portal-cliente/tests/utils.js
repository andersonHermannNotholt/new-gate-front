/* eslint-disable import/no-extraneous-dependencies */
import Vuex from 'vuex';
import { createLocalVue } from '@vue/test-utils';
import { cloneDeep } from 'lodash';

export function getStore(modules) {
  const localVue = createLocalVue();
  localVue.use(Vuex);

  return new Vuex.Store(cloneDeep({
    modules,
  }));
}
