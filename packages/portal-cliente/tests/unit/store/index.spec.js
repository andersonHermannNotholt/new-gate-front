import storeModule from '@/store';
import notify from '@/lib/notify';
import views from '@/store/views';
import login from '@/store/login';
import app from '@/store/app';
import createLocalStore from './createStore';

describe('store', () => {
  it('exports a store', () => {
    const store = createLocalStore();
    expect(store).toBeDefined();
  });

  it('add services modules', () => {
    const service = jest.fn();
    const session = jest.fn();
    const client = {
      service: () => ({}),
    };
    storeModule({ client, service, session });
    expect(service).toHaveBeenNthCalledWith(1, 'users');
    expect(service).toHaveBeenNthCalledWith(2, 'users/passwordResets');
    expect(service).toHaveBeenNthCalledWith(3, 'users/passwordChanges');
    expect(service).toHaveBeenNthCalledWith(4, 'users/emailSubscriptions');
    expect(service).toHaveBeenNthCalledWith(5, 'tokens');
    expect(service).toHaveBeenNthCalledWith(6, 'accounts');
    expect(service).toHaveBeenNthCalledWith(7, 'mobileRequests');
  });

  it('add plugins', () => {
    const service = jest.fn();
    const session = jest.fn();
    const client = {
      service: () => ({}),
    };
    const store = storeModule({
      client,
      service,
      session,
    });
    expect(store.plugins).toContain(notify);
    expect(store.plugins).toContain(session);
  });

  it('add modules', () => {
    const service = jest.fn();
    const session = jest.fn();
    const client = {
      service: () => ({}),
    };
    const store = storeModule({
      client,
      service,
      session,
    });
    expect(store.modules).toMatchObject({
      login,
      app,
      views,
    });
  });
});
